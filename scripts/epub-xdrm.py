#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
"""
Modify Un-encrypted DRM epub as no DRM epub file

Basically, hide the files that match DRM_FILES from the epub zip file
"""

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
import zipfile
import shutil

NATIVE=sys.getfilesystemencoding()

DRM_FILES = ["META-INF/rights.xml", "META-INF/encryption.xml"]

def zip_underline_file(zipfile, path_list):
    """Mark files in path_list with extra underline """
    infolist = zipfile.infolist()
    namelist = {x.filename: x for x in infolist}
    for path in path_list:
        if path in namelist:
            namelist[path].filename = path + "_"
            zipfile._didModify = True
    #namelist = {x.filename: x for x in infolist}

def fix_drm(fname):
    base, ext = os.path.splitext(fname)
    outname = "{}-xdrm{}".format(base, ext)
    print('Fix DRM for "{}" as "{}" ...'.format(fname, outname))
    shutil.copy(fname, outname)
    with zipfile.ZipFile(outname, "a") as azip:
        zip_underline_file(azip, DRM_FILES)

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    file_list = sys.argv[1:]
    for fname in file_list:
        fix_drm(fname)

if __name__ == '__main__':
    main()

