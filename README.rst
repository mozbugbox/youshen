YouShen 有神
============

| Copyright (C) 2009-2020, Mozbugbox <mozbugbox@yahoo.com.au>
| Release Under GNU GPL version 3 or later version.

读书破万卷，下笔如有神

Document reader/viewer for PDF, epub files.

The PyMuPDF_ library was used to render the documents.

The intent of this application was to create a *hackable* PDF reader.
Features should be easily added to the reader, hence Python_ was chosen as the
programming language.

Patches/Pull Requests are welcomed and appreciated!

Features
========

* Apply a background color over the white background for easy reading.

* Crop out white border of document pages.

* A small indicator to show the last scrolling position when scrolling pages.

* Document formated with two columns can be read easily by reading one
  column a time. Then scroll down to the next column.

Dependency
==========

* Python_ 3

* MuPDF_ / PyMuPDF_

* `GTK+`_ / PyGObject_


Configuration
=============

Configuration files are stored in ``$HOME/.local/share/youshen``

Extras
======

``epub-xdrm.py`` is a simple script to hide encryption xml in a epub file.
The MuPDF library won't read epub with encryption information.

Homepage
========

* URL: https://gitlab.com/mozbugbox/youshen
* Wiki: https://gitlab.com/mozbugbox/youshen/wikis/home


.. _Python: https://www.python.org/
.. _MuPDF: https://mupdf.com/
.. _PyMuPDF: https://github.com/pymupdf/PyMuPDF
.. _PyGObject: https://wiki.gnome.org/Projects/PyGObject
.. _GTK+: http://www.gtk.org/
