#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

class TreeMatcher:
    def __init__(self):
        self.im_matcher = None

    def treeview_equal_func(self, model, col, akey, aiter, *data):
        """For treeview search, search for 'in'. """
        val = model.get_value(aiter, col).lower()
        akey = akey.lower()
        if self.im_matcher is None:
            ret = akey not in val
        else:
            ret = not self.im_matcher.contain(val, akey)
        return ret

    def load_im_matcher(self, im_json=None):
        if im_json is None:
            im_json = "im-matcher-table.json"
        from . import const
        for data_dir in ["", const.USER_DATA_DIR, const.PKG_DATA_DIR,]:
            data_path = os.path.join(data_dir, im_json)
            if os.path.exists(data_path):
                break
        else:
            log.warn("IM-Matcher data file not found: {}".format(im_json))
            return

        try:
            import immatcher
            self.im_matcher = immatcher.create_matcher(data_path)
        except ImportError:
            log.warn("Failed to import 'immatcher' module")

tree_matcher = TreeMatcher()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

