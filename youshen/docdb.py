#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
import time

NATIVE=sys.getfilesystemencoding()

class DocumentDB:
    def __init__(self, path):
        self.path = path
        self.setting_tab = "settings"
        self.doc_tab = "document_list"
        self.config_tab = "config"
        self.con = self.setup_database()
        self.house_keeping_interval = 10
        self._house_keeping_id = None
        self._pending_commit = False
        self.old_document_timeout = 1 * 365 * 24 * 3600 # 1 years
        self.purge_document_interval = 30 * 24 * 3600
        self.purge_deadline = time.time() + self.purge_document_interval

    def setup_database(self):
        import sqlite3
        con = sqlite3.connect(self.path)
        if not os.path.exists(self.path):
            con.execute("PRAGMA page_size = 4096;")
        con.execute('''CREATE TABLE if not exists {} (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            path TEXT)'''.format(self.doc_tab))
        con.execute('''CREATE TABLE IF NOT EXISTS {} (
            doc_id INTEGER,
            key TEXT,
            value TEXT,
            FOREIGN KEY(doc_id) REFERENCES {}(id)
            )'''.format(self.setting_tab, self.doc_tab))
        #con.execute("DROP TABLE config")
        con.execute('''CREATE TABLE if not exists {} (
            key TEXT UNIQUE,
            value ANY
            )'''.format(self.config_tab))
        con.commit()
        return con

    def execute(self, cmd, *args, **kwargs):
        """proxy sqlite execute method"""
        log.debug(f"{cmd}, {args}, {kwargs}")
        cur = self.con.cursor()
        ret = cur.execute(cmd, *args, **kwargs)
        return ret

    def exe_commit(self, cmd, *args, **kwargs):
        """execute and then commit sqlite command"""
        log.debug(f"{cmd}, {args}, {kwargs}")
        ret = self.execute(cmd, *args, **kwargs)
        self.con.commit()
        return ret

    def executemany(self, cmd, *args, **kwargs):
        """proxy sqlite executemany method"""
        log.debug(f"{cmd}, {args}, {kwargs}")
        cur = self.con.cursor()
        ret = cur.executemany(cmd, *args, **kwargs)
        return ret

    def get_pragma(self, pragma):
        """Get database PRAGMA values"""
        cur = self.con.cursor()
        ret = cur.execute("PRAGMA {};".format(pragma)).fetchall()
        return ret

    def queue_commit(self):
        self._pending_commit = True

    def commit(self):
        self._pending_commit = False
        self.con.commit()

    def document_list(self):
        """Get a list of all documents"""
        cur = self.con.cursor()
        cur.execute("SELECT id, path FROM {}".format(self.doc_tab))
        return list(cur.fetchall())

    def document_new(self, doc_path, **settings):
        cur = self.con.cursor()
        cur.execute('INSERT INTO {} (title) VALUES (?)'.format(self.doc_tab),
                (doc_path,))
        doc_id = cur.lastrowid
        if settings:
            data = [[doc_id] + list(x) for x in settings.items()]
            cur.executemany(
                'INSERT INTO {} (doc_id, key, value) VALUES (?,?,?)'.format(
                self.setting_tab), data)
        self.document_timestamp(doc_id)
        self.con.commit()
        return doc_id

    def get_doc_id(self, doc_id):
        if isinstance(doc_id, str):
            cur = self.con.cursor()
            cur.execute("SELECT id FROM {} WHERE path = ?".format(
                self.doc_tab), (doc_id,))
            res = cur.fetchone()
            if res:
                doc_id = res[0]
        return doc_id

    def document_move(self, doc_id, new_path):
        cur = self.con.cursor()
        doc_id = self.get_doc_id(doc_id)

        cur.execute('UPDATE {} SET path=? WHERE id=?'.format(
            self.doc_tab), (new_path, doc_id))
        self.document_timestamp(doc_id)
        self.queue_commit()

    def document_setting_get(self, doc_id):
        cur = self.con.cursor()
        doc_id = self.get_doc_id(doc_id)
        cur.execute('SELECT key, value FROM {} WHERE doc_id = ?'.format(
            self.setting_tab), (doc_id,))
        self.document_timestamp(doc_id)
        return dict(cur.fetchall())

    def document_setting_set(self, doc_id, **settings):
        cur = self.con.cursor()
        doc_id = self.get_doc_id(doc_id)
        old_settings = self.document_setting_get(doc_id)
        has_keys = {x:y for x,y in settings.items()
                if x in old_settings and y != old_settings[x]}
        no_keys = {x:y for x,y in settings.items() if x not in old_settings}

        if has_keys:
            data = [list(reversed(x)) + [doc_id] for x in has_keys.items()]
            cur.executemany('''UPDATE {} SET value = ?
                    WHERE key = ? AND doc_id=?'''.format(
                self.setting_tab), data)
        if no_keys:
            data = [[doc_id] + list(x) for x in no_keys.items()]
            cur.executemany('''INSERT INTO {} (doc_id, key, value)
            VALUES (?,?,?)'''.format(self.setting_tab), data)

        if len(has_keys) > 0 or len(no_keys) > 0:
            self.document_timestamp(doc_id)
            self.queue_commit()

    def document_setting_new(self, doc_id, key, value):
        cur = self.con.cursor()
        doc_id = self.get_doc_id(doc_id)
        cur.execute('''
                INSERT INTO {} (doc_id, key, value) VALUES (?,?,?)
                '''.format(
            self.setting_tab), (doc_id, key, value))
        self.document_timestamp(doc_id)
        self.queue_commit()

    def document_setting_delete(self, doc_id, *keys):
        doc_id = self.get_doc_id(doc_id)
        data = [(x, doc_id) for x in keys]
        cur = self.con.cursor()
        cur.executemany('DELETE FROM {} WHERE key = ? AND  doc_id=?'.format(
            self.setting_tab), data)
        self.document_timestamp(doc_id)
        self.queue_commit()

    def document_delete(self, doc_ids):
        doc_ids = [self.get_doc_id(x) for x in doc_ids]
        data = [(x,) for x in doc_ids]
        cur = self.con.cursor()
        cur.executemany('DELETE FROM {} WHERE id=?'.format(
            self.doc_tab), data)
        cur.executemany('DELETE FROM {} WHERE doc_id=?'.format(
            self.setting_tab), data)
        self.queue_commit()

    def document_timestamp(self, *doc_ids):
        doc_ids = [self.get_doc_id(x) for x in doc_ids]
        now = time.time()
        cur = self.con.cursor()
        data = [[now, "update_time"] + [x]  for x in doc_ids]
        cur.executemany('''UPDATE {} SET value=?
                WHERE key = ? AND doc_id=?'''.format(
            self.setting_tab), data)
        self.queue_commit()

    def save_config(self, config):
        cur = self.con.cursor()
        #data = [(x, str(y)) for x, y in config.items()]
        data = config.items()
        cur.executemany('''INSERT OR REPLACE INTO {}
                (key, value)
                VALUES(?, ?)
                '''.format(
            self.config_tab, self.config_tab), data)
        self.queue_commit()

    def load_config(self):
        cur = self.con.cursor()
        cur.execute('SELECT key, value FROM {}'.format(self.config_tab))

        return {x[0]: x[1] for x in cur.fetchall()}

    def _do_commit(self):
        if self._pending_commit == True:
            self.commit()

    def _do_purge_removed_document(self):
        now = time.time()
        if now < self.purge_deadline:
            return
        self.purge_deadline = now + self.purge_document_interval

        deadline = now - self.old_document_timeout
        cur = self.con.cursor()
        cur.execute('SELECT doc_id, value FROM {} WHERE key = ?'.format(
            self.setting_tab), ("update_time",))
        res = cur.fetchall()

        old_docs = []
        for doc_id, value in res:
            if float(value) < deadline:
                old_docs.append(doc_id)

        exists = []
        non_exists = []
        cur.execute("SELECT id, path FROM {} WHERE id in ({})".format(
            self.doc_tab, ", ".join("?"*len(old_docs))), old_docs)
        for doc_id, path in cur.fetchall():
            if os.path.exists(path):
                exists.append(doc_id)
            else:
                non_exists.append(doc_id)
        self.delete_document(non_exists)
        self.document_timestamp(*exists)

    def do_house_keeping(self):
        self._do_commit()
        self._do_purge_removed_document()
        return True

    def start_commit_monitor(self):
        from gi.repository import GLib
        if self._house_keeping_id is not None:
            GLib.source_remove(self._house_keeping_id)
        self._house_keeping_id = GLib.timeout_add_seconds(
                self.house_keeping_interval, self.do_house_keeping)

    def try_vacuum(self):
        """Try to vacuum the database when meet some threshold"""
        cur = self.con.cursor()
        page_count = self.get_pragma("page_count")[0][0]
        freelist_count = self.get_pragma("freelist_count")[0][0]
        page_size = self.get_pragma("page_size")[0][0]

        #print(page_count, freelist_count, page_count - freelist_count)
        # 25% freepage and 1MB wasted space
        if (float(freelist_count)/page_count > .25
                and freelist_count * page_size > 1024*1024):
            cur.execute("VACUUM;")
            self.commit()

    def close(self):
        self.commit()
        self.try_vacuum()
        self.con.close()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

