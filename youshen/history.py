#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
"""
  Manage Navigation History
  Copyright (C) 2016 mozbugbox <mozbugbox@yahoo.com.au>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
from gi.repository import GObject

NATIVE=sys.getfilesystemencoding()

class History(GObject.GObject):
    """Navigation history"""
    __gsignals__ = {
            # callback(history, "action")
            'changed': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
            }
    def __init__(self, old=[], new=[]):
        GObject.GObject.__init__(self)
        self.old = old
        self.new = new
        self._undo = False


    def undo(self, current_index):
        """Return None if failed to undo"""
        if not self.can_undo:
            return None
        val = self.old.pop()
        while val == current_index:
            if not self.can_undo:
                self.push_old(val)
                return None
            val = self.old.pop()
        self._undo = True
        self.emit("changed", "undo")
        return val

    def redo(self, current_index):
        """Return None if failed to redo"""
        if not self.can_redo:
            return None
        val = self.new.pop()
        while val == current_index:
            if not self.can_redo:
                self.push_new(val)
                return None
            val = self.new.pop()
        self._undo = False
        self.emit("changed", "redo")
        return val

    def reset_undo_flag(self):
        if self._undo:
            self._undo = False


    def on_page_changed(self, old_page, new_page):
        """Handle page changed action"""
        if abs(old_page - new_page) > 1:
            if self._undo:
                self.push_new(old_page)
                self.push_new(new_page)
            else:
                self.push_old(old_page)
                self.push_old(new_page)
            self.emit("changed", "page_changed")
        self.reset_undo_flag()

    @property
    def can_undo(self):
        return len(self.old)

    @property
    def can_redo(self):
        return len(self.new)

    @property
    def state(self):
        """Return current history state"""
        data = {"old": self.old, "new": self.new }
        return data

    @state.setter
    def state(self, data):
        """Set current history state"""
        if data:
            self.old = data["old"]
            self.new = data["new"]
        else:
            self.old = []
            self.new = []
        self.emit("changed", "state")

    def clear(self):
        self.state = None
        self.emit("changed", "clear")

    def push_new(self, val):
        # prevent redandant insertion into history line
        if ((not self.new or self.new[-1] != val)
                and (not self.old or self.old[-1] != val)):
            self.new.append(val)
            return True

    def push_old(self, val):
        # prevent redandant insertion into history line
        if ((not self.new or self.new[-1] != val)
                and (not self.old or self.old[-1] != val)):
            self.old.append(val)
            return True

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

