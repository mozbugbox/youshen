#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
import os

__version__ = "0.1.0"
# APPNAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
APPNAME = "YouShen"
__version_id__ = "{} {}".format(APPNAME, __version__)
APPID = "{}.{}.{}".format("org", "mozbugbox", APPNAME)

UI_FILE = "youshen.ui"
MENU_FILE = "menu.ui"

PKG_DIR = os.path.dirname(__file__)
PKG_DATA_DIR = os.path.join(PKG_DIR, 'data')

USER_DATA_DIR = os.path.expandvars(os.path.join("$HOME/.local/share",
    APPNAME.lower()))
if not os.path.isdir(USER_DATA_DIR):
    os.makedirs(USER_DATA_DIR)
USER_DATABASE = os.path.join(USER_DATA_DIR, "document.sqlite")

# color palette defaults are from Gtk+ ColorChooserWidget
default_palette = [
        "#EF2929", "#CC0000", "#A40000",  # Scarlet Red
        "#FCAF3E", "#F57900", "#CE5C00",  # Orange
        "#FCE94F", "#EDD400", "#C4A000",  # Butter
        "#8AE234", "#63C216", "#4E9A06",  # Chameleon
        "#729FCF", "#3465A4", "#204A87",  # Sky Blue
        "#AD7FA8", "#75507B", "#5C3566",  # Plum
        "#736F84", "#504E64", "#414153",  # Mid Night
        "#737C41", "#646B3A", "#545A30",  # Earthy Green
        "#708090", "#6E7F80", "#536878",  # Blue Grey
        "#9D8863", "#82744B", "#6B5D30",  # Earthy Yellow
        ]

default_gray_palette = [
        "#000000",  # black
        "#2E3436",  # very dark gray
        "#555753",  # darker gray
        "#6A6C68",  # darker gray
        "#888A85",  # dark gray
        "#BABDB6",  # medium gray
        "#D3D7CF",  # light gray
        "#EEEEEC",  # lighter gray
        "#F3F3F3",  # very light gray
        "#FFFFFF",  # white
        ]

background_palette = [
        # green
        "#66AABB", "#66BBB0", "#66AA99",
        "#408080", "#408070", "#408050",
        "#407040", "#227788", "#006070",
        "#225566", "#206060",
        # blue
        "#6688AA", "#2080E0", "#3066D0",
        "#43617A", "#234F80", "#22639D",
        "#3C415E", "#2E3868",
        # yellow
        "#A19058", "#807049",
        "#8B9070", "#887755", "#706660",
        # red
        "#402A51", "#453056", "#69313E",
        "#732323", "#7D3F31", "#93483B",
        ]

